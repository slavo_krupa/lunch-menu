# Lunch menu parser

## What can I use?
* ``localhost:8080/rest/v1/menus/all/`` - return menu for current date
* `localhost:8080/rest/v1/menus/all/date/2019-11-05` - return menu for given date

## Testing
Run `./gradlew clean test` to run all the tests

## Running locally
Create the directory `/app/lib/conf` and add a file called 'jwt-secret.conf'
```hocon
jwt.secret = 1234-1234-1234-1234
```
Run `./gradlew :app:run` to run the service

Inspired by ["Hexagonal Architecture with Kotlin, Ktor and Guice" article](https://hackernoon.com/hexagonal-architecture-with-kotlin-ktor-and-guice-f1b68fbdf2d9) and [and it's repository](https://github.com/sgerber-hyperanna/ktor-hexagonal-multi-module-template).
