package dev.slavo.lunchmenu.domain.parsers

import com.google.common.base.Splitter
import dev.slavo.lunchmenu.domain.parsers.exceptions.DateParseException
import dev.slavo.lunchmenu.domain.util.StateFulIterator
import dev.slavo.lunchmenu.domain.util.extensions.isMisTypedInString
import dev.slavo.lunchmenu.domain.util.extensions.isWrittenInString
import dev.slavo.lunchmenu.domain.util.extensions.iterator
import dev.slavo.lunchmenu.domain.util.extensions.workingDays
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.provided.RestaurantParser
import dev.slavo.lunchmenu.ports.provided.dto.Food
import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import java.time.DayOfWeek
import java.time.LocalDate
import javax.inject.Inject


class AnnapurnaParser
@Inject
constructor(
    private val contentRetriever: ContentRetrievers.AnnaPurna
) : RestaurantParser {
    private class Parser(
        private val contentIterator: StateFulIterator<String>
    ) {
        private val restaurantMenus = ArrayList<RestaurantMenu>()
        private val weekDates = mutableMapOf<DayOfWeek, LocalDate>()
        private val type = Restaurant.ANNAPURNA
        private var partialResult = RestaurantMenu(type)
        fun parse(): List<RestaurantMenu> {
            parseDate()
            parseDescription()
            parseMenu()
            return restaurantMenus
        }

        //        pondeli/monday
//        cena bez polévky  cena s polévkou
//        Tomato Soup/chicken soup (0,2l),rajská polévka / Kuřeci polévka
//        1chicken  curry150g kuřecí kousky na kari omačkou                99-113,-
//        2 keema madras150g,jehneci mleti maso s palivou omačkou99-113,-
//        3 alupumpkin mango 50g,brambory a dynes  omačkou chuti mango
//        4 Chana masala150g, indická cizrna s omáčkou                         99-113,-
//        5 Mix Thali 200g, mix dnniho menu                                      134,-148,-
//        Úterý /tuesday

        private fun parseMenu() {
            var line = contentIterator.next()
            do {
                val dayOfWeek = workingDays()
                    .firstOrNull { dayOfWeek -> dayOfWeek.isWrittenInString(line) || dayOfWeek.isMisTypedInString(line) }
                if (dayOfWeek != null) {
                    val foods = ArrayList<Food>()
                    do {
                        line = contentIterator.next()
                        if (line.containsMenu()) {
                            foods.add(Food(line))
                        }
                    } while (contentIterator.hasNext() && (line.containsMenu() || line.isWithinDailyMenu()))
                    val computedDate = weekDates.getOrElse(dayOfWeek) { throw DateParseException(type) }
                    restaurantMenus.add(partialResult.copy(date = computedDate, foods = foods))
                } else {
                    line = contentIterator.next()
                }
            } while (contentIterator.hasNext())
        }

        private fun String.containsMenu() = this[0].isDigit() || this.contains("soup")
        // cena bez polévky  cena s polévkou
        //  www.indicka-restaurace-annapurna.cz
        // Polední menu podáváme do 15 hodin
        // Těšíme se na Vaši návštěvu a přejeme Vám dobrou chuť
        private fun String.isWithinDailyMenu() = this.contains("cena")

        /**
         *
        ".
        Jídelní lístek – týdenní menu
        Jako přílohu můžete vybrat  rýže nebo placka nebo napůl
        Kuřecí polévka každý den
        "
         */
        private fun parseDescription() {
            // skip two lines
            contentIterator.next()
            contentIterator.next()
            partialResult = partialResult.copy(other = contentIterator.next() + " " + contentIterator.next())

        }

        private fun parseDate() {
            content@ for (line in contentIterator)
            // e.g. "{04.03-08.03.2019}"
                if (line.contains(Regex(DATE_FORMAT_REGEX))) {
                    val numbersAndSeparators = PARSABLE_DATE_REGEX.replace(line, "")
                    @Suppress("UnstableApiUsage")
                    val dates = Splitter.on("-")
                        .omitEmptyStrings()
                        .trimResults()
                        .splitToList(numbersAndSeparators)
                    check(dates.size == 2)
                    val to = LocalDate.parse(dates[1], EUROPE_DATE_FORMAT)
                    // 04.03 -> 04.03.2019
                    val from = LocalDate.parse("${dates[0]}.${to.year}", EUROPE_DATE_FORMAT)
                    for (date in from..to) {
                        weekDates[date.dayOfWeek] = date
                    }
                    check(weekDates.keys.size == 5)
                    break@content
                }
        }
    }


    // Annapurna have problems with DNS while downloading PDF file
    override fun getWeeklyMenu(): List<RestaurantMenu> {

        val response = contentRetriever.getContent()
        PDDocument.load(response.binaryArray).use { document ->
            val text = PDFTextStripper().getText(document)

            val results = Splitter.onPattern("\r?\n")
                .omitEmptyStrings()
                .trimResults()
                .split(text)
            return Parser(StateFulIterator(results)).parse()
        }
    }
}
