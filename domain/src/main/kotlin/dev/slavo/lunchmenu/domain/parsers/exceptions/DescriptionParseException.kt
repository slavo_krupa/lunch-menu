package dev.slavo.lunchmenu.domain.parsers.exceptions

import dev.slavo.lunchmenu.ports.both.Restaurant

class DescriptionParseException(type: Restaurant) : RuntimeException("Unable to parse $type description.")
