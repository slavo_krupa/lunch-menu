package dev.slavo.lunchmenu.domain.parsers

import com.google.inject.AbstractModule
import com.google.inject.multibindings.MapBinder
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.provided.RestaurantParser

class ParsersModule : AbstractModule() {
    override fun configure() {
        val mapBinder = MapBinder.newMapBinder(binder(), Restaurant::class.java, RestaurantParser::class.java)
        mapBinder.addBinding(Restaurant.ANNAPURNA).to(AnnapurnaParser::class.java)
        mapBinder.addBinding(Restaurant.BUDDHA).to(BuddhaParser::class.java)
        mapBinder.addBinding(Restaurant.HAPPY_TEA_HOUSE).to(HappyTeaHouseParser::class.java)
        mapBinder.addBinding(Restaurant.POKHARA).to(PokharaParser::class.java)
    }
}