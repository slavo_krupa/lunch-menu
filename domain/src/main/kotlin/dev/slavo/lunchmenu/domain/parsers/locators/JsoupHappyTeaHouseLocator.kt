package dev.slavo.lunchmenu.domain.parsers.locators

import dev.slavo.lunchmenu.ports.provided.locator.HappyTeaHouseLocator
import dev.slavo.lunchmenu.ports.required.dto.HtmlContent
import org.jsoup.Jsoup

class JsoupHappyTeaHouseLocator : HappyTeaHouseLocator {
    override fun locatePdfInHtml(content: HtmlContent): String {
        Jsoup.parse(content.htmlText).run {
            val pdfElement = select(".pdfemb-viewer").firstOrNull()
            return pdfElement?.attr("href").toString()
        }
    }
}