package dev.slavo.lunchmenu.domain.util

/**
 *Simple class that allows file processing with remembered state
 */
data class StateFulIterator<T>(
    private val content: Iterable<T>
) :
    Iterable<T>,
    Iterator<T> {
    var current: T? = null
        private set
    private val iterator: Iterator<T> = content.iterator()
    override fun hasNext(): Boolean = iterator.hasNext() || current != null

    fun takeWhile(condition: (T) -> Boolean): List<T> {
        val result = mutableListOf<T>()
        doWhile(condition) { result.add(it) }
        return result
    }

    fun doWhile(condition: (T) -> Boolean, action: (T) -> Unit) {
        while (hasNext() && condition(peek())) {
            action(next())
        }
    }

    fun peek(): T {
        current = current ?: iterator.next()
        return current!!
    }

    override fun next(): T {
        val backup = current
        return if (backup != null) {
            current = null
            backup
        } else {
            iterator.next()
        }
    }

    override fun iterator(): Iterator<T> {
        return this
    }


}