package dev.slavo.lunchmenu.domain.parsers

import com.google.common.base.Splitter
import dev.slavo.lunchmenu.domain.util.CurrentWeekDays
import dev.slavo.lunchmenu.domain.util.StateFulIterator
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.provided.RestaurantParser
import dev.slavo.lunchmenu.ports.provided.dto.Food
import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import javax.inject.Inject

class HappyTeaHouseParser
@Inject
constructor(
    private val contentRetriever: ContentRetrievers.HappyTeaHouse,
    private val currentWeekDays: CurrentWeekDays
) : RestaurantParser {

    override fun getWeeklyMenu(): List<RestaurantMenu> {
        val response = contentRetriever.getContent()
        PDDocument.load(response.binaryArray).use { document ->
            val text = PDFTextStripper().getText(document)

            val results = Splitter.onPattern("\r?\n")
                .omitEmptyStrings()
                .trimResults()
                .split(text)
            return Parser(StateFulIterator(results), currentWeekDays).parse()
        }
    }

    class Parser(
        private val stateFulIterator: StateFulIterator<String>,
        private val currentWeekDays: CurrentWeekDays
    ) {
        fun parse(): List<RestaurantMenu> {
            // skip Ob?dové menu
            stateFulIterator.next()
            val menus: MutableList<RestaurantMenu> = ArrayList(5)
            val description = stateFulIterator.next()
            repeat(5) { menus.add(parseDailyMenu(description)) }
            return menus
        }

        private fun parseDailyMenu(description: String): RestaurantMenu {
            val czechDay = stateFulIterator.next()
            val foods: MutableList<Food> = ArrayList(6)
            foods.add(soup())
            repeat(5) { foods.add(mainCourse()) }
            return RestaurantMenu(Restaurant.HAPPY_TEA_HOUSE, currentWeekDays.getDateForCzechDate(czechDay), foods, description)
        }

        private fun soup(): Food = Food(stateFulIterator.next())

        private fun mainCourse(): Food =
            Food(stateFulIterator.next() + " " + stateFulIterator.next())
    }
}