package dev.slavo.lunchmenu.domain.util.extensions

import java.time.DayOfWeek
import java.time.LocalDate

fun LocalDate.beginWeekDate(): LocalDate {
    val daysDifference = dayOfWeek.value - DayOfWeek.MONDAY.value
    return minusDays(daysDifference.toLong())
}

fun LocalDate.endWorkWeekDate(): LocalDate {
    val daysDifference = DayOfWeek.FRIDAY.value - dayOfWeek.value
    return plusDays(daysDifference.toLong())
}