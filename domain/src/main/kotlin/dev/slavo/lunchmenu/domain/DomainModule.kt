package dev.slavo.lunchmenu.domain

import com.google.inject.AbstractModule
import dev.slavo.lunchmenu.domain.parsers.ParsersModule
import dev.slavo.lunchmenu.domain.service.SimpleMenuService
import dev.slavo.lunchmenu.ports.provided.Greeter
import dev.slavo.lunchmenu.ports.provided.MenuService

class DomainModule : AbstractModule() {
    override fun configure() {
        bind(Greeter::class.java).to(SimpleGreeter::class.java)
        bind(MenuService::class.java).to(SimpleMenuService::class.java)
        install(ParsersModule())
    }
}
