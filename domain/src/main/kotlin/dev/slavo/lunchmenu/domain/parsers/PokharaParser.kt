package dev.slavo.lunchmenu.domain.parsers

import dev.slavo.lunchmenu.domain.util.StateFulIterator
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.provided.RestaurantParser
import dev.slavo.lunchmenu.ports.provided.dto.Food
import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.time.LocalDate
import javax.inject.Inject

class PokharaParser
@Inject
constructor(
    private val contentRetriever: ContentRetrievers.Pokhara
) : RestaurantParser {
    override fun getWeeklyMenu(): List<RestaurantMenu> {
        val nonEmptyElements = ArrayList<Element>()
        val main = Jsoup.parse(contentRetriever.getContent().htmlText)
            .select("main").first() ?: throw IllegalArgumentException("No 'main' element")
        main.allElements
            .filter { !it.ownText().isNullOrEmpty() && !it.hasClass("contentheader") }
            .forEach { nonEmptyElements.add(it) }

        return Parser(StateFulIterator(nonEmptyElements)).read()
    }

    private class Parser(
        private val stateFulIterator: StateFulIterator<Element>
    ) {

        fun read(): List<RestaurantMenu> {
            val menus = ArrayList<RestaurantMenu>()
            while (stateFulIterator.hasNext() && stateFulIterator.peek().`is`("strong")) {
                menus.add(parseDay())
            }
            val description = stateFulIterator.takeWhile { true }.map { it.ownText() }.joinToString(separator = " ")
            return menus.map { it.copy(other = description) }
        }

        private fun parseDay(): RestaurantMenu {
            val date = parseDate()
            val foodLines = stateFulIterator.takeWhile { it.`is`("p") && it.parent().`is`("td") }
            val foods = ArrayList<Food>(5)
            check(foodLines.size % 2 == 0) { "Menu is formatted by table. Menu and price are in different cells." }
            val linesIterator = foodLines.iterator()
            while (linesIterator.hasNext()) {
                foods.add(Food(contactTwoNextElements(linesIterator)))
            }
            return RestaurantMenu(Restaurant.POKHARA, date, foods)
        }

        private fun parseDate(): LocalDate {
            val dateString = stateFulIterator.takeWhile { it.`is`("strong") }.map { it.ownText() }.joinToString(separator = "")
            // Úterý 5.11.2019
            return LocalDate.parse(dateString.split(" ")[1], EUROPE_DATE_FORMAT_SINGLE_DIGIT)
        }

        private fun contactTwoNextElements(iterator: Iterator<Element>): String = iterator.next().ownText() + " " + iterator.next().ownText()

    }
}