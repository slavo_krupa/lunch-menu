package dev.slavo.lunchmenu.domain.util

import dev.slavo.lunchmenu.domain.util.extensions.CzechDayOfWeek
import java.time.Clock
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.ZoneId

class CurrentWeekDays(private val clock: Clock = Clock.systemDefaultZone()) {

    fun getDateForWeekDay(day: DayOfWeek): LocalDate {
        val now = LocalDate.ofInstant(clock.instant(), ZoneId.systemDefault())
        return if (now.dayOfWeek > day) {
            now.minusDays((now.dayOfWeek.value - day.value).toLong())
        } else {
            now.plusDays((day.value - now.dayOfWeek.value).toLong())
        }
    }

    fun getDateForCzechDate(czechDayName: String): LocalDate = getDateForWeekDay(CzechDayOfWeek.translateToDayOfWeek(czechDayName))

}