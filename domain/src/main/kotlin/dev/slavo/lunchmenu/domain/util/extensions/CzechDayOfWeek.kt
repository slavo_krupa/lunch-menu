package dev.slavo.lunchmenu.domain.util.extensions

import org.apache.commons.lang3.StringUtils
import java.time.DayOfWeek

enum class CzechDayOfWeek(private val englishDayOfWeek: DayOfWeek) {
    PONDELI(DayOfWeek.MONDAY),
    UTERY(DayOfWeek.TUESDAY),
    STREDA(DayOfWeek.WEDNESDAY),
    CTVRTEK(DayOfWeek.THURSDAY),
    PATEK(DayOfWeek.FRIDAY),
    ;

    companion object {
        fun translateToDayOfWeek(dayName: String): DayOfWeek {
            val normalizedUpperCaseName = StringUtils.stripAccents(dayName).toUpperCase()
            return values().find { normalizedUpperCaseName == it.name }?.englishDayOfWeek
                ?: throw IllegalArgumentException("Can not find :'${dayName}' in czech week days")
        }
    }


}