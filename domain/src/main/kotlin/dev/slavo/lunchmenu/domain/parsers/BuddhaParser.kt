package dev.slavo.lunchmenu.domain.parsers

import dev.slavo.lunchmenu.domain.parsers.exceptions.DateParseException
import dev.slavo.lunchmenu.domain.parsers.exceptions.DescriptionParseException
import dev.slavo.lunchmenu.domain.parsers.exceptions.MenuParsingException
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.provided.RestaurantParser
import dev.slavo.lunchmenu.ports.provided.dto.Food
import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import org.jsoup.Jsoup
import java.time.LocalDate
import javax.inject.Inject


class BuddhaParser
@Inject constructor(
    private val contentRetriever: ContentRetrievers.Buddha
) : RestaurantParser {
    private val type = Restaurant.BUDDHA


    override fun getWeeklyMenu(): List<RestaurantMenu> {
        val menus = ArrayList<RestaurantMenu>(5)
        Jsoup.parse(contentRetriever.getContent().htmlText).run {
            select(".textmenu").forEach {
                val date = parseDate(it.select(".vyrazne").text())
                val foods: List<Food> = parseMenus(it.text())
                val description: String = parseDescription(it.text())
                menus.add(RestaurantMenu(type, date, foods, description))
            }
            return menus
        }
    }

    private fun parseDate(text: String?): LocalDate {
//        PONDĚLÍ /Monday/ 25. 2.:
        val withoutText =
            text?.substring(text.lastIndexOf('/') + 1, text.lastIndexOf('.'))
                ?.replace(Regex("\\s"), "")
                ?.split('.')
                ?: throw DateParseException(type)
        val day = withoutText[0].toInt()
        val month = withoutText[1].toInt()
        val year = LocalDate.now().year
        return LocalDate.of(year, month, day)

    }

    //        PONDĚLÍ /Monday/ 25. 2.: * Polévka: 0,2l Chicken Soup (kuřecí polévka, A: 1) 22,- Kč
    //        * 150g Chicken Korma (kuřecí kousky ve smet. omáčce s kešu&kokosem, A: 7+8) 99,- Kč
    //        * 150g Chicken Fal (kuřecí kousky ve velmi ostré omáčce Fal, A: 7) 99,- Kč
    //        * 1.VEG 150g Dal Palak (hrachový Dál se špenátem, A: 7) 99,- Kč
    //        * 2.VEG 150g Aloo Chana Masala (brambory a cizrna v aromat. omáčce Masala, A: 7) 99,- Kč
    //        * 150g Mix Thali (mix dnešního menu na děleném talíři, A: 7) 130,- Kč
    //        ALERGENY: 1: obiloviny (lepek), 3: vejce, 7: mléko/ml. výrobky a 8: skořápkové plody (kešu+kokos). Polévky obsahují glutaman a mouku.
    private fun parseDescription(text: String?): String {
        return text?.substring(descriptionStartIndex(text), text.length) ?: throw DescriptionParseException(type)
    }

    private fun descriptionStartIndex(text: String) = text.lastIndexOf("ALERGENY")

    private fun parseMenus(text: String?): List<Food> {
        return text
            // remove last lines starting with "ALERGENY"
            ?.substring(0, descriptionStartIndex(text))
            //every menu starts with *
            ?.split("*")
            // remove "PONDĚLÍ /Monday/ 25. 2.:"
            ?.drop(1)?.map { Food(it) }
            ?: throw MenuParsingException(type)
    }


}