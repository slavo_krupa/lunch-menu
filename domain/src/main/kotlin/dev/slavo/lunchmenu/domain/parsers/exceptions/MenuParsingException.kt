package dev.slavo.lunchmenu.domain.parsers.exceptions

import dev.slavo.lunchmenu.ports.both.Restaurant

class MenuParsingException(type: Restaurant) : RuntimeException("Unable to parse $type food menu items.")
