package dev.slavo.lunchmenu.domain.parsers.exceptions

enum class WorkingWeekDays {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY;


}
