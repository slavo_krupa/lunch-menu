package dev.slavo.lunchmenu.domain.parsers.exceptions

import dev.slavo.lunchmenu.ports.both.Restaurant

// TODO this should be fixed by enforcing mapping between Content and Location
class UnexpectedContentException(val restaurant: Restaurant) :
    RuntimeException("Wrong content for  $restaurant. Seem like programmer issue.")
