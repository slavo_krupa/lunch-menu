package dev.slavo.lunchmenu.domain.parsers

import java.time.format.DateTimeFormatter


internal const val DATE_FORMAT_REGEX = "\\d{2}(\\.|-)\\d{2}(\\.|-)\\d{4}"
internal val PARSABLE_DATE_REGEX = Regex("[^0-9.-]")

internal val EUROPE_DATE_FORMAT_SINGLE_DIGIT = DateTimeFormatter.ofPattern("d.M.yyyy")
internal val EUROPE_DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy")
internal val EUROPE_DATE_WITHOUT_YEAR_FORMAT = DateTimeFormatter.ofPattern("dd.MM")

