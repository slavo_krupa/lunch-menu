package dev.slavo.lunchmenu.domain.service

import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.provided.MenuService
import dev.slavo.lunchmenu.ports.provided.RestaurantParser
import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.provided.rest.DailyRestaurantsMenu
import dev.slavo.lunchmenu.ports.provided.rest.SingleRestaurant
import dev.slavo.lunchmenu.ports.required.RestaurantMenuRepository
import dev.slavo.lunchmenu.ports.required.dto.RestaurantDateKey
import java.time.LocalDate
import javax.inject.Inject

class SimpleMenuService
@Inject constructor(
    //TODO: use set instead Map
    private val restaurantParsers: Map<Restaurant, @JvmSuppressWildcards RestaurantParser>,
    private val repository: RestaurantMenuRepository
) : MenuService {
    //    TODO should be done in block and all other methods should be suspendable as part of coroutines to improve performance
    override fun getAllMenus(date: LocalDate): DailyRestaurantsMenu {
        val restaurantsList = enumValues<Restaurant>()
            .map {
                getSingleRestaurantMenu(RestaurantDateKey(date, it))
            }.map { SingleRestaurant(it) }
            .toList()

        return DailyRestaurantsMenu(date, restaurantsList)
    }

    override fun getSingleRestaurantMenu(key: RestaurantDateKey): RestaurantMenu {
        return repository.find(key)
            .orElseGet { parseForDate(key) }
    }

    private fun parseForDate(key: RestaurantDateKey): RestaurantMenu {
        val menus = getRestaurantContent(key)
        menus.forEach { repository.store(it) }
        return menus.firstOrNull { it hasSameValueAs key } ?: RestaurantMenu(key.restaurant, date = key.date)
    }

    private fun getRestaurantContent(key: RestaurantDateKey): List<RestaurantMenu> {
        try {
            val parser = restaurantParsers.getOrElse(key.restaurant) { throw IllegalStateException("Missing restaurant parser binding.") }
            return parser.getWeeklyMenu()
        } catch (e: Exception) {
            return emptyList()
        }
    }
}