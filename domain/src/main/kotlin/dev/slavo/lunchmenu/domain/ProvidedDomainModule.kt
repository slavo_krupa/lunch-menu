package dev.slavo.lunchmenu.domain

import com.google.inject.AbstractModule
import dev.slavo.lunchmenu.domain.parsers.locators.JsoupHappyTeaHouseLocator
import dev.slavo.lunchmenu.ports.provided.locator.HappyTeaHouseLocator

/**
 * This module should contain classes that are required by adapters and doesn't require any adapters.
 */
class ProvidedDomainModule : AbstractModule() {
    override fun configure() {
        bind(HappyTeaHouseLocator::class.java).to(JsoupHappyTeaHouseLocator::class.java)
    }
}