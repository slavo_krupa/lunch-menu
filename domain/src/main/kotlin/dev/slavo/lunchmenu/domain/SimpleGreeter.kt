package dev.slavo.lunchmenu.domain

import dev.slavo.lunchmenu.ports.provided.Greeter
import dev.slavo.lunchmenu.ports.required.GreetingsRepository
import javax.inject.Inject

class SimpleGreeter @Inject constructor(private val greeterRepository: GreetingsRepository) :
    Greeter {
    override fun greet(username: String): String {
        val greetingForUser = greeterRepository.findGreetingForUser(username)
        return "Hello from the domain module. Here is your personalized message: '$greetingForUser'"
    }
}
