package dev.slavo.lunchmenu.domain.util.extensions

import java.time.DayOfWeek
import java.util.stream.IntStream

fun DayOfWeek.isWrittenInString(text: String): Boolean = text.contains(this.name, true)
/**
 * We can allow one mistake in name typing
 */
fun DayOfWeek.isMisTypedInString(text: String): Boolean {
    return IntStream.rangeClosed(0, name.length)
        .mapToObj { name.removeRange(it, it) }
        .anyMatch { text.contains(it) }
}

fun workingDays(): List<DayOfWeek> = (DayOfWeek.MONDAY..DayOfWeek.FRIDAY).toList()

fun ClosedRange<DayOfWeek>.toList(): List<DayOfWeek> {
    val list = ArrayList<DayOfWeek>()
    for (dayOfWeek in this) {
        list.add(dayOfWeek)
    }
    return list
}


operator fun ClosedRange<DayOfWeek>.iterator(): Iterator<DayOfWeek> {
    return object : Iterator<DayOfWeek> {
        private var iterator = start
        private val finalElement = endInclusive

        override fun hasNext(): Boolean = iterator <= finalElement

        override fun next(): DayOfWeek {
            require(hasNext())
            val value = iterator
            iterator += 1
            return value

        }

    }

}

