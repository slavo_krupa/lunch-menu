package dev.slavo.lunchmenu.domain.parsers

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import dev.slavo.lunchmenu.ports.required.dto.HtmlContent
import io.kotlintest.assertSoftly
import io.kotlintest.matchers.date.shouldBeAfter
import io.kotlintest.matchers.date.shouldBeBefore
import io.kotlintest.matchers.string.beEmpty
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class BuddhaParserTest {
    private val contentRetriever: ContentRetrievers.Buddha = mock()
    private val buddhaParser = BuddhaParser(contentRetriever)


    @Test
    fun getSingleRestaurantMenu() {
        whenever(contentRetriever.getContent())
            .thenReturn(HtmlContent(TestUtils.getResourceContentAsString("buddha.html")))
        val startDate = LocalDate.of(2019, 2, 24)
        val endDate = startDate.plusDays(6)
        val menu = buddhaParser.getWeeklyMenu()
        assertSoftly {
            menu.size shouldBe 5
            menu.forEach { menu ->
                menu.date shouldBeAfter startDate
                menu.date shouldBeBefore endDate
                menu.foods.forEach { food ->
                    food.description shouldNotBe beEmpty()
                }
                menu.other shouldNotBe beEmpty()
            }
        }
        verify(contentRetriever).getContent()
    }
}