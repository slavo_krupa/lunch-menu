package dev.slavo.lunchmenu.domain.util.extensions

import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test
import java.time.DayOfWeek

internal class CzechDayOfWeekTest {
    @Test
    fun upperCaseName_translate_Monday() {
        CzechDayOfWeek.translateToDayOfWeek("PONDELI") shouldBe DayOfWeek.MONDAY
    }

    @Test
    fun lowerCaseName_translate_Monday() {
        CzechDayOfWeek.translateToDayOfWeek("pondeli") shouldBe DayOfWeek.MONDAY
    }

    @Test
    fun czechCharactersName_translate_Monday() {
        CzechDayOfWeek.translateToDayOfWeek("Pondělí") shouldBe DayOfWeek.MONDAY
    }
}