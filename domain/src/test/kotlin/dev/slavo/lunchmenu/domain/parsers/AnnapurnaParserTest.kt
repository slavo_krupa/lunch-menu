package dev.slavo.lunchmenu.domain.parsers

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import dev.slavo.lunchmenu.ports.required.dto.PdfContent
import io.kotlintest.assertSoftly
import io.kotlintest.matchers.date.shouldBeAfter
import io.kotlintest.matchers.date.shouldBeBefore
import io.kotlintest.matchers.string.beEmpty
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class AnnapurnaParserTest {
    private val contentRetriever: ContentRetrievers.AnnaPurna = mock()
    private val annapurna = AnnapurnaParser(contentRetriever)

    @Test
    fun getSingleRestaurantMenu() {
        whenever(contentRetriever.getContent())
            .thenReturn(PdfContent(TestUtils.getResourceContentAsByteArray("annapurna.pdf")))
        val menu = annapurna.getWeeklyMenu()
        menu.forEach {
            println(it.date.dayOfWeek)
        }

        val startDate = LocalDate.of(2019, 3, 3)
        val endDate = startDate.plusDays(6)

        assertSoftly {
            menu.size shouldBe 4
            menu.forEach { menu ->
                menu.restaurant shouldBe Restaurant.ANNAPURNA
                menu.date shouldBeAfter startDate
                menu.date shouldBeBefore endDate
                menu.foods.forEach { food ->
                    food.description shouldNotBe beEmpty()
                }
                menu.other shouldNotBe beEmpty()
            }
        }
        verify(contentRetriever).getContent()
    }
}