package dev.slavo.lunchmenu.domain.util

import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test
import java.time.*

internal class CurrentWeekDaysTest {

    private val `Wednesday 27 November 1974 07 33 20` = 154766000000
    private val weekDays = CurrentWeekDays(Clock.fixed(Instant.ofEpochMilli(`Wednesday 27 November 1974 07 33 20`), ZoneId.systemDefault()))

    @Test
    internal fun wednesday_wantMonday_MondayFromGivenWeek() {
        weekDays.getDateForWeekDay(DayOfWeek.MONDAY) shouldBe LocalDate.of(1974, 11, 25)
    }

    @Test
    internal fun wednesday_wantWednesday_wednesdayFromGivenWeek() {
        weekDays.getDateForWeekDay(DayOfWeek.WEDNESDAY) shouldBe LocalDate.of(1974, 11, 27)
    }

    @Test
    internal fun wednesday_wantFriday_fridayFromGivenWeek() {
        weekDays.getDateForWeekDay(DayOfWeek.WEDNESDAY) shouldBe LocalDate.of(1974, 11, 27)
    }
}