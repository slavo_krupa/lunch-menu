package dev.slavo.lunchmenu.domain.parsers.locators

import dev.slavo.lunchmenu.domain.parsers.TestUtils
import dev.slavo.lunchmenu.ports.required.dto.HtmlContent
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test

internal class JsoupHappyTeaHouseLocatorTest {

    @Test
    fun locatePdfInHtml() {
        val content = HtmlContent(TestUtils.getResourceContentAsString("happyTeaHouseSource.html"))
        val locatePdfInHtml = JsoupHappyTeaHouseLocator().locatePdfInHtml(content)
        locatePdfInHtml shouldBe "http://www.veselacajovna.cz/wp-content/uploads/2019/11/listopad.pdf"
    }
}