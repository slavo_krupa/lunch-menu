package dev.slavo.lunchmenu.domain.parsers

import org.apache.commons.io.IOUtils
import java.io.InputStream

object TestUtils {
    fun getResourceStream(resourceName: String): InputStream =
        TestUtils::class.java.classLoader.getResourceAsStream(resourceName) ?: throw IllegalArgumentException(resourceName)


    fun getResourceContentAsString(resourceName: String): String =
        IOUtils.toString(getResourceStream(resourceName), Charsets.UTF_8)

    fun getResourceContentAsByteArray(resourceName: String): ByteArray =
        IOUtils.toByteArray(getResourceStream(resourceName))
}