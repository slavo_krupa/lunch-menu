package dev.slavo.lunchmenu.domain.util

import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import org.junit.jupiter.api.Test
import java.util.Collections.singletonList

internal class StateFulIteratorTest {

    @Test
    internal fun emptyList_constructor_doesntFail() {
        StateFulIterator(emptyList<Int>())
    }

    @Test
    internal fun emptyList_peek_fail() {
        shouldThrow<NoSuchElementException> {
            StateFulIterator(emptyList<Int>()).peek()
        }
    }

    @Test
    internal fun emptyList_next_fail() {
        shouldThrow<NoSuchElementException> {
            StateFulIterator(emptyList<Int>()).next()
        }
    }

    @Test
    internal fun emptyList_hasNext_false() {
        StateFulIterator(emptyList<Int>()).hasNext() shouldBe false
    }

    @Test
    internal fun singleItemList_hasNext_true() {
        StateFulIterator(singletonList(1)).hasNext() shouldBe true
    }

    @Test
    internal fun singleItemList_peekMultipleTimes_sameValue() {
        val stateFulIterator = StateFulIterator(singletonList(1))
        stateFulIterator.peek() shouldBe 1
        stateFulIterator.peek() shouldBe 1
    }

    @Test
    internal fun singleItemList_peekAndNext_sameValue() {
        val stateFulIterator = StateFulIterator(singletonList(1))
        stateFulIterator.peek() shouldBe 1
        stateFulIterator.next() shouldBe 1
    }

    @Test
    internal fun singleItemList_peekAndHasNext_shouldHaveNext() {
        val stateFulIterator = StateFulIterator(singletonList(1))
        stateFulIterator.peek() shouldBe 1
        stateFulIterator.hasNext() shouldBe true
    }

    @Test
    internal fun multiItemList_next_shouldIterateOverElements() {
        val stateFulIterator = StateFulIterator(listOf(1, 2, 3))
        stateFulIterator.next() shouldBe 1
        stateFulIterator.next() shouldBe 2
        stateFulIterator.next() shouldBe 3
        shouldThrow<NoSuchElementException> {
            stateFulIterator.next()
        }
    }

    @Test
    internal fun multiItemList_foreach_shouldIterateOverElements() {
        val source = listOf(1, 2, 3)
        val stateFulIterator = StateFulIterator(source)
        val result = mutableListOf<Int>()
        stateFulIterator.forEach { result.add(it) }
        result shouldContainExactly source
    }

    @Test
    internal fun multiItemList_takeWhile_shouldStopAt5() {
        val stateFulIterator = StateFulIterator(listOf(1, 1, 5, 1))
        stateFulIterator.takeWhile { it < 5 } shouldHaveSize 2
    }

    @Test
    internal fun multiItemList_doWhile_shouldStopAt5() {
        val stateFulIterator = StateFulIterator(listOf(1, 1, 5, 1))
        val result = mutableListOf<Int>()
        stateFulIterator.doWhile({ it < 5 }) {
            result.add(it)
        }
        result shouldHaveSize 2
    }


}