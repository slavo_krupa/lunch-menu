package dev.slavo.lunchmenu.domain.parsers

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import dev.slavo.lunchmenu.ports.required.dto.HtmlContent
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.matchers.string.shouldContainIgnoringCase
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import java.time.LocalDate

internal class PokharaParserTest {

    private val contentRetriever = mock(ContentRetrievers.Pokhara::class.java)
    private val parser = PokharaParser(contentRetriever)

    @Test
    fun getWeeklyMenu() {
        whenever(contentRetriever.getContent()) doReturn HtmlContent(TestUtils.getResourceContentAsString("pokhara/menu.html"))
        val weeklyMenu = parser.getWeeklyMenu()
        weeklyMenu shouldHaveSize 5
        weeklyMenu[0].date shouldBe LocalDate.of(2019, 11, 4)
        weeklyMenu[0].foods[0].description.shouldContain("Rajčatová polévka (Tomato soup) ")
        weeklyMenu[2].foods[2].description.shouldContainIgnoringCase("chicken butter")
    }
}