package dev.slavo.lunchmenu.domain.parsers

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import dev.slavo.lunchmenu.domain.util.CurrentWeekDays
import dev.slavo.lunchmenu.domain.util.extensions.beginWeekDate
import dev.slavo.lunchmenu.domain.util.extensions.endWorkWeekDate
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import dev.slavo.lunchmenu.ports.required.dto.PdfContent
import io.kotlintest.assertSoftly
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class HappyTeaHouseParserTest {

    private val retriever: ContentRetrievers.HappyTeaHouse = mock()
    private val parser = HappyTeaHouseParser(retriever, CurrentWeekDays())

    @Test
    fun getSingleRestaurantMenu() {
        val content = PdfContent(TestUtils.getResourceContentAsByteArray("happyTeaHouse/Menu_unor_4_2019.pdf"))
        whenever(retriever.getContent()) doReturn content
        val weeklyMenu = parser.getWeeklyMenu()
        weeklyMenu shouldHaveSize 5
        weeklyMenu.forEach {
            it.foods shouldHaveSize 6
        }
        assertSoftly {
            weeklyMenu[0].restaurant shouldBe Restaurant.HAPPY_TEA_HOUSE
            weeklyMenu[0].other shouldBe "Rýže/Naan (1,3,7) a polévka k menu zdarma"
            weeklyMenu[0].date shouldBe LocalDate.now().beginWeekDate()
            weeklyMenu[0].foods[0].description.shouldContain("Hrášková")
            weeklyMenu[0].foods[1].description.shouldContain("Omáčka z čočky")
            weeklyMenu[0].foods[1].description.shouldContain("Dal Fry")
            weeklyMenu[4].date shouldBe LocalDate.now().endWorkWeekDate()
        }
    }
}