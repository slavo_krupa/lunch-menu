plugins {
    kotlin("jvm")
}

dependencies {
    compile(project(":ports"))
    compile("org.jsoup:jsoup:1.11.3")
    compile("commons-io:commons-io:2.6")
    compile("org.apache.pdfbox:pdfbox:2.0.13")
    compile("org.apache.commons:commons-lang3:3.9")
    testCompile("io.kotlintest:kotlintest-assertions:3.1.10")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.1.0")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.0.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.1.0")
}
