package dev.slavo.lunchmenu.adapters

import com.google.inject.AbstractModule
import dev.slavo.lunchmenu.ports.provided.Greeter
import dev.slavo.lunchmenu.ports.provided.MenuService
import dev.slavo.lunchmenu.ports.provided.locator.HappyTeaHouseLocator
import org.mockito.Mockito.mock

class DomainMocksModule : AbstractModule() {
    override fun configure() {
        bind(Greeter::class.java).toInstance(mock(Greeter::class.java))
        bind(MenuService::class.java).toInstance(mock(MenuService::class.java))
        bind(HappyTeaHouseLocator::class.java).toInstance(mock(HappyTeaHouseLocator::class.java))
    }
}
