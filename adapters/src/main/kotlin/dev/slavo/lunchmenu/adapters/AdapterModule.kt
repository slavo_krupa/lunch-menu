package dev.slavo.lunchmenu.adapters

import com.google.inject.AbstractModule
import dev.slavo.lunchmenu.adapters.content.ContentProviderModule
import dev.slavo.lunchmenu.adapters.db.InMemoryGreeterRepository
import dev.slavo.lunchmenu.adapters.db.InMemoryRestaurantMenuRepository
import dev.slavo.lunchmenu.ports.required.GreetingsRepository
import dev.slavo.lunchmenu.ports.required.RestaurantMenuRepository
import io.ktor.util.KtorExperimentalAPI

class AdapterModule : AbstractModule() {

    @KtorExperimentalAPI
    override fun configure() {
        // N.B. - order matters, application config needs to be loaded first
        bind(ApplicationConfig::class.java).asEagerSingleton()
        bind(Routes::class.java).asEagerSingleton()
        bind(GreetingsRepository::class.java).to(InMemoryGreeterRepository::class.java)
        bind(RestaurantMenuRepository::class.java).to(InMemoryRestaurantMenuRepository::class.java).asEagerSingleton()
        install(ContentProviderModule())
    }
}
