package dev.slavo.lunchmenu.adapters.content

import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetriever
import dev.slavo.lunchmenu.ports.required.dto.HtmlContent
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.coroutines.runBlocking

open class HtmlContentRetriever(val url: String) :
    ContentRetriever<HtmlContent> {
    override fun getContent(): HtmlContent {
        return runBlocking {
            return@runBlocking HtmlContent(HttpClient().get(url))
        }
    }
}