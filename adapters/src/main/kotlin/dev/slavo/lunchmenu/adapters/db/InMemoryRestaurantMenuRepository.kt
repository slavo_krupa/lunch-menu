package dev.slavo.lunchmenu.adapters.db

import com.google.common.cache.CacheBuilder
import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.required.RestaurantMenuRepository
import dev.slavo.lunchmenu.ports.required.dto.RestaurantDateKey
import java.util.*
import java.util.concurrent.TimeUnit


class InMemoryRestaurantMenuRepository : RestaurantMenuRepository {

    private val storage = CacheBuilder.newBuilder()
        .maximumSize(Restaurant.values().size * 10L)
        .expireAfterWrite(1, TimeUnit.DAYS)
        .build<RestaurantDateKey, RestaurantMenu>()

    override fun store(menu: RestaurantMenu) {
        storage.put(RestaurantDateKey(menu.date, menu.restaurant), menu)
    }

    override fun find(restaurantDateKey: RestaurantDateKey): Optional<RestaurantMenu> =
        Optional.ofNullable(storage.getIfPresent(restaurantDateKey))


}