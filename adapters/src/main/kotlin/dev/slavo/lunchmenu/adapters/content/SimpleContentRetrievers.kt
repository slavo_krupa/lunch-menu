package dev.slavo.lunchmenu.adapters.content

import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers

class SimpleContentRetrievers {
    class AnnaPurna :
        PdfContentRetriever("http://indicka-restaurace-annapurna.cz/images/tydenijidelnicek/menu.pdf"),
        ContentRetrievers.AnnaPurna

    class Budha : HtmlContentRetriever("http://www.indian-restaurant-buddha.cz/"), ContentRetrievers.Buddha
    class Pokhara : HtmlContentRetriever("https://www.pokhara.cz/tydenni-menu/"), ContentRetrievers.Pokhara
}