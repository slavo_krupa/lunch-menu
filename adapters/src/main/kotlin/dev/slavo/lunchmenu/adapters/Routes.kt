package dev.slavo.lunchmenu.adapters

import com.google.inject.Inject
import dev.slavo.lunchmenu.ports.provided.Greeter
import dev.slavo.lunchmenu.ports.provided.MenuService
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.auth.jwt.JWTPrincipal
import io.ktor.http.ContentType
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import java.time.LocalDate

class Routes @Inject constructor(application: Application, greeter: Greeter, menuService: MenuService) {
    init {
        application.routing {
            get("/") {
                call.respondText(greeter.greet("anonymous"), contentType = ContentType.Text.Plain)
            }
            get("/rest/v1/menus/all/{date?}") {
                val dateString = this.call.parameters["date"]
                val date =
                    if (dateString == null) {
                        LocalDate.now()
                    } else {
                        LocalDate.parse(dateString)
                    }
                call.respond(menuService.getAllMenus(date))
            }

            get("/json/gson") {
                call.respond(mapOf("hello" to "world"))
            }

            authenticate {
                route("/secure/hello") {
                    handle {
                        val principal = call.authentication.principal<JWTPrincipal>()
                        val subjectString = principal!!.payload.subject
                        call.respondText(greeter.greet(subjectString), contentType = ContentType.Text.Plain)
                    }
                }
            }
        }
    }
}
