package dev.slavo.lunchmenu.adapters.content

import com.google.inject.AbstractModule
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import io.ktor.util.KtorExperimentalAPI

class ContentProviderModule : AbstractModule() {
    @KtorExperimentalAPI
    override fun configure() {
        bind(ContentRetrievers.AnnaPurna::class.java).to(SimpleContentRetrievers.AnnaPurna::class.java)
        bind(ContentRetrievers.HappyTeaHouse::class.java).to(HappyTeaHouseContentRetriever::class.java)
        bind(ContentRetrievers.Buddha::class.java).to(SimpleContentRetrievers.Budha::class.java)
        bind(ContentRetrievers.Pokhara::class.java).to(SimpleContentRetrievers.Pokhara::class.java)
    }
}