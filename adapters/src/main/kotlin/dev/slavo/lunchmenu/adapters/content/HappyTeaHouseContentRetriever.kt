package dev.slavo.lunchmenu.adapters.content

import dev.slavo.lunchmenu.ports.provided.locator.HappyTeaHouseLocator
import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetrievers
import dev.slavo.lunchmenu.ports.required.dto.PdfContent
import javax.inject.Inject
import javax.inject.Provider

class HappyTeaHouseContentRetriever
@Inject
constructor(
    private val locator: Provider<HappyTeaHouseLocator>
) : ContentRetrievers.HappyTeaHouse {
    private val htmlRetriever = HtmlContentRetriever("http://www.veselacajovna.cz/tydenni-nabidka/")
    override fun getContent(): PdfContent {
        val pdfUrl = locator.get().locatePdfInHtml(htmlRetriever.getContent())
        return PdfContentRetriever(pdfUrl).getContent()
    }
}