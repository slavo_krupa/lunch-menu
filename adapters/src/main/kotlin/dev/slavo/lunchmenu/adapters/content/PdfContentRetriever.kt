package dev.slavo.lunchmenu.adapters.content

import dev.slavo.lunchmenu.ports.required.content.retrievers.ContentRetriever
import dev.slavo.lunchmenu.ports.required.dto.PdfContent
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.http.Url
import io.ktor.util.cio.toByteArray
import kotlinx.coroutines.runBlocking
import java.io.File


open class PdfContentRetriever(val url: String) :
    ContentRetriever<PdfContent> {
    override fun getContent(): PdfContent {
        return runBlocking {
            val response = HttpClient().get<HttpResponse>(url)
            val content = response.content.toByteArray()
            File("C:\\work\\${Url(url).host}.pdf").writeBytes(content)
            return@runBlocking PdfContent(content)
        }

    }

}