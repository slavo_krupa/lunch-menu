package dev.slavo.lunchmenu.adapters.db

import dev.slavo.lunchmenu.ports.required.GreetingsRepository

class InMemoryGreeterRepository : GreetingsRepository {
    override fun findGreetingForUser(user: String): String {
        return when (user) {
            "alice" -> "Why hello!"
            "anna" -> "Nice to meet you!"
            "bob" -> "Salute"
            else -> "Don't know you, $user"
        }
    }
}
