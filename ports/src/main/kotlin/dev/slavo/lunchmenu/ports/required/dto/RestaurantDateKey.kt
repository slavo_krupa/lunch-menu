package dev.slavo.lunchmenu.ports.required.dto

import dev.slavo.lunchmenu.ports.both.Restaurant
import java.time.LocalDate

data class RestaurantDateKey(
    val date: LocalDate,
    val restaurant: Restaurant
)