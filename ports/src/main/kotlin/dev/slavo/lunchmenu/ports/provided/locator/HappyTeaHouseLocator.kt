package dev.slavo.lunchmenu.ports.provided.locator

import dev.slavo.lunchmenu.ports.required.dto.HtmlContent

interface HappyTeaHouseLocator {
    fun locatePdfInHtml(content: HtmlContent): String
}