package dev.slavo.lunchmenu.ports.required.dto

sealed class Content
data class HtmlContent(val htmlText: String) : Content()
data class PdfContent(val binaryArray: ByteArray) : Content()


