package dev.slavo.lunchmenu.ports.required

interface GreetingsRepository {
    fun findGreetingForUser(user: String): String
}
