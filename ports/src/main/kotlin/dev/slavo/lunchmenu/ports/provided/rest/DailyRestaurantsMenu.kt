package dev.slavo.lunchmenu.ports.provided.rest

import java.time.LocalDate

data class DailyRestaurantsMenu(
    val date: LocalDate,
    val restaurants: List<SingleRestaurant>
)