package dev.slavo.lunchmenu.ports.provided.dto

data class Food(
    val description: String = ""
)