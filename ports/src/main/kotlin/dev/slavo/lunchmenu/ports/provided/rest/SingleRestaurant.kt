package dev.slavo.lunchmenu.ports.provided.rest

import dev.slavo.lunchmenu.ports.provided.dto.Food
import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu

data class SingleRestaurant(
    val name: String,
    val foods: List<Food>,
    val notes: String
) {
    constructor(menu: RestaurantMenu) : this(menu.restaurant.restaurantName, menu.foods, menu.other)
}

