package dev.slavo.lunchmenu.ports.provided

import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.provided.rest.DailyRestaurantsMenu
import dev.slavo.lunchmenu.ports.required.dto.RestaurantDateKey
import java.time.LocalDate

interface MenuService {
    fun getAllMenus(date: LocalDate): DailyRestaurantsMenu
    fun getSingleRestaurantMenu(key: RestaurantDateKey): RestaurantMenu
}