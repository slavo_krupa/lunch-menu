package dev.slavo.lunchmenu.ports.required

import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu
import dev.slavo.lunchmenu.ports.required.dto.RestaurantDateKey
import java.util.*

interface RestaurantMenuRepository {
    fun store(menu: RestaurantMenu)
    fun find(restaurantDateKey: RestaurantDateKey): Optional<RestaurantMenu>
}