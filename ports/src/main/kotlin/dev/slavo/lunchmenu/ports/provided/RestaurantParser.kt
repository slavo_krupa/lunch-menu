package dev.slavo.lunchmenu.ports.provided

import dev.slavo.lunchmenu.ports.provided.dto.RestaurantMenu

interface RestaurantParser {
    fun getWeeklyMenu(): List<RestaurantMenu>
}