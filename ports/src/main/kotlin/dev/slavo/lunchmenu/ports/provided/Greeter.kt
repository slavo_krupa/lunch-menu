package dev.slavo.lunchmenu.ports.provided

interface Greeter {
    fun greet(username: String): String
}
