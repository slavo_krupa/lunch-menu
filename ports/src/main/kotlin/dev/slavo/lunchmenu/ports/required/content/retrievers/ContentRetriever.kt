package dev.slavo.lunchmenu.ports.required.content.retrievers

import dev.slavo.lunchmenu.ports.required.dto.Content

interface ContentRetriever<C : Content> {
    fun getContent(): C
}



