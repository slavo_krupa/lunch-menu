package dev.slavo.lunchmenu.ports.required.content.retrievers

import dev.slavo.lunchmenu.ports.required.dto.HtmlContent
import dev.slavo.lunchmenu.ports.required.dto.PdfContent

class ContentRetrievers {
    //region Implementations
    interface Buddha : ContentRetriever<HtmlContent>

    interface AnnaPurna : ContentRetriever<PdfContent>
    interface Pokhara : ContentRetriever<HtmlContent>
    interface HappyTeaHouse : ContentRetriever<PdfContent>
    //endregion
}