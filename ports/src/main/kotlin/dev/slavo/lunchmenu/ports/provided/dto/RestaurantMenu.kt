package dev.slavo.lunchmenu.ports.provided.dto

import dev.slavo.lunchmenu.ports.both.Restaurant
import dev.slavo.lunchmenu.ports.required.dto.RestaurantDateKey
import java.time.LocalDate


data class RestaurantMenu(
    val restaurant: Restaurant,
    val date: LocalDate = LocalDate.now(),
    val foods: List<Food> = emptyList(),
    val other: String = ""
) {
    infix fun hasSameValueAs(key: RestaurantDateKey): Boolean = key.date == date && key.restaurant == restaurant
}
