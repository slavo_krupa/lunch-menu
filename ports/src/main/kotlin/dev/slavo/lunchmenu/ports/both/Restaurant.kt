package dev.slavo.lunchmenu.ports.both

enum class Restaurant(val restaurantName: String) {
    ANNAPURNA("Annapurna"),
    BUDDHA("Buddha"),
    HAPPY_TEA_HOUSE("Veselá čajovňa"),
    POKHARA("Pokhara"),
    ;
}